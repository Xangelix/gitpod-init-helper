#!/usr/bin/env bash

read -p "Template: " sel_template
read -p "Git Address: " sel_gitaddr

curl -fsSL https://gitlab.com/Xangelix/gitpod-signed-workspace/-/raw/master/.gitpod.init.sh -o ./.gitpod.init.sh; sudo chmod +x ./.gitpod.init.sh; ./.gitpod.init.sh $sel_gitaddr $sel_template
